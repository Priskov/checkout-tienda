import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Info } from 'src/app/interfaces/interfaces';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})

export class HomePage implements OnInit {
  obj:Info=this.data.getInfo()

  email:string=this.obj.email || ""
  nombre:string=this.obj.nombre || ""
  apellido:string=this.obj.apellido || ""
  pais:string=this.obj.pais ||"Selecciona pais"
  provincia:string=this.obj.provincia ||"Provincia"
  direccion:string=this.obj.direccion ||""
  telefono:string=this.obj.telefono || ""

  @Input()

  emailError:boolean=false

  
  constructor(private router:Router,private data:DataService) {  }
  
  ngOnInit() {
  }

  

  next(){
    
    let obj:Info={
      nombre:this.nombre,
      apellido:this.apellido,
      email:this.email,
      pais:this.pais,
      provincia:this.provincia,
      direccion:this.direccion,
      telefono:this.telefono,
      gratis:"",
      pago:""
    }
    
    this.data.setInfo(obj)
    this.router.navigate(["segunda"])
  }


}

