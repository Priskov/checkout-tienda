import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CuartaPagina } from './cuarta.page';

const routes: Routes = [
  {
    path: '',
    component: CuartaPagina
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CuartaRoutingModule {}
