import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Info } from 'src/app/interfaces/interfaces';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-cuarta',
  templateUrl: './cuarta.page.html',
  styleUrls: ['./cuarta.page.scss'],
})

export class CuartaPagina implements OnInit {
  @Input()

  obj:Info=this.data.getInfo()

  nombre:string=this.obj.nombre
  apellido:string=this.obj.apellido
  direccion:string=this.obj.direccion
  email:string=this.obj.email
  pais:string=this.obj.pais
  provincia:string=this.obj.provincia
  telefono:string=this.obj.telefono
  gratis:string=this.obj.gratis
  pago:string=this.obj.pago
  

  constructor(private router:Router,private data:DataService) { }

  ngOnInit() {
  }

}