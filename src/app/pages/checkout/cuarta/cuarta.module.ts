import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CuartaRoutingModule } from './cuarta-routing.module';

import { CuartaPagina as Cuarta } from './cuarta.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CuartaRoutingModule,
    ComponentsModule
  ],
  declarations: [Cuarta]
})
export class CuartaModule {}