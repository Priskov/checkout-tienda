import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Segunda } from './segunda.page';

describe('Segunda', () => {
  let component: Segunda;
  let fixture: ComponentFixture<Segunda>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ Segunda ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Segunda);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
