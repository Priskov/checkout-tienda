import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SegundaRoutingModule } from './segunda-routing.module';

import { Segunda } from './segunda.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SegundaRoutingModule,
    ComponentsModule
  ],
  declarations: [Segunda]
})
export class SegundaModule {}
