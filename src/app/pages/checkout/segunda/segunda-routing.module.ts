import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Segunda } from './segunda.page';

const routes: Routes = [
  {
    path: '',
    component: Segunda
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SegundaRoutingModule {}
