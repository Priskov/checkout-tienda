import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Info } from 'src/app/interfaces/interfaces';
import { DataService } from 'src/app/services/data.service';


@Component({
  selector: 'app-segunda',
  templateUrl: './segunda.page.html',
  styleUrls: ['./segunda.page.scss'],
})

export class Segunda implements OnInit {
  @Input()
  
  obj:Info=this.data.getInfo()

  email:string=this.obj.email
  nombre:string=this.obj.nombre || ""
  apellido:string=this.obj.apellido || ""
  pais:string=this.obj.pais || ""
  provincia:string=this.obj.provincia || ""
  direccion:string=this.obj.direccion || ""
  telefono:string=this.obj.telefono || ""
  gratis:string=this.obj.gratis




  constructor(private router:Router,private data:DataService) { }

  back(){
    this.router.navigate(['home'])
  }
  
  next(){
    const newobj:Info={
      ...this.obj,
      gratis:this.gratis,
    }
    this.data.setInfo(newobj)
    this.router.navigate(['tercera'])

  }

  ngOnInit() {
  }
  

}
