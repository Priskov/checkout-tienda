import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Info } from 'src/app/interfaces/interfaces';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-tercera',
  templateUrl: './tercera.page.html',
  styleUrls: ['./tercera.page.scss'],
})

export class Tercera implements OnInit {
  @Input()

  obj:Info= this.data.getInfo()
  pago:string



  constructor(private router:Router,private data:DataService) { }

 
  ngOnInit() {
  }

  volver(){
    this.router.navigate(['segunda'])
  }
  
  siguiente(){
    const newobj:Info={
      ...this.obj,
      pago:this.pago
    }
    this.data.setInfo(newobj)
    this.router.navigate(['cuarta'])
  }
}
