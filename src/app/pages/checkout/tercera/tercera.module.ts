import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TerceraRoutingModule } from './tercera-routing.module';

import { Tercera as TerceraPagina } from './tercera.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TerceraRoutingModule,
    ComponentsModule
  ],
  declarations: [TerceraPagina]
})
export class TerceraModule {}
