import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Tercera as Tercera } from './tercera.page';

const routes: Routes = [
  {
    path: '',
    component: Tercera
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TerceraRoutingModule {}
