import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { TotalComponent } from './total/total.component';



@NgModule({
  declarations: [
    HeaderComponent,
    TotalComponent
  ],
  exports:[
    HeaderComponent,
    TotalComponent
  ],
  imports: [
    CommonModule
  ]
})
export class ComponentsModule { }
