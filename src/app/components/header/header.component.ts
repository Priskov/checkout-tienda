import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  comp:string

  constructor(private router: Router) {
    if(router.url==="/home"){
      this.comp="home"
    }
    if(router.url==="/segunda"){
      this.comp="segunda"
    }
    if(router.url==="/tercera"){
      this.comp="tercera"
    }
    if(router.url==="/cuarta"){
      this.comp="cuarta"
    }
   }

  ngOnInit() {}

}
