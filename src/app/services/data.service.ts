import { Injectable } from '@angular/core';
import { Info } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})

export class DataService {

  private info:Info={
    nombre:"",
    apellido:"",
    direccion:"",
    email:"",
    pais:"",
    provincia:"",
    telefono:"",
    gratis:"",
    pago:""
  }

  constructor() { }

  setInfo(obj:Info){
    this.info=obj
  }

  getInfo():Info{
    return this.info
  }


}
