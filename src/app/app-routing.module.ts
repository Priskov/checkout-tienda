import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
 
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
 
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  
  {
    path: 'segunda',
    loadChildren: () => import('./pages/checkout/segunda/segunda.module').then( m => m.SegundaModule)
  },
 
  {
    path: 'tercera',
    loadChildren: () => import('./pages/checkout/tercera/tercera.module').then( m => m.TerceraModule)
  },
  {
    path: 'cuarta',
    loadChildren: () => import('./pages/checkout/cuarta/cuarta.module').then( m => m.CuartaModule)
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
