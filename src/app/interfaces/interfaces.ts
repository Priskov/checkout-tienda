export interface Info{
    nombre:string,
    apellido:string,
    direccion:string,
    email:string,
    pais:string,
    provincia:string,
    telefono:string,
    gratis:string,
    pago:string,
  }